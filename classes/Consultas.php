<?php
class Consultas{

	function __construct() {

      $this->db = new MysqliDb ('localhost', 'root', 'Abc12345', 'prueba');

    }
	
	public function ListarTablas(){
    	$tablas = $this->db->rawQuery('SHOW TABLES');
    	return $tablas;
    }

    public function ListaCamposTabla($tabla){
		$columnas = $this->db->rawQuery('SHOW COLUMNS FROM '.$tabla);
    	return $columnas;
    }

    public function Consulta($datos){
        $data = [];
        $columnas = '';
        foreach ($datos as $key=> $dato) {
            $campo_tabla = explode('.', $dato);
            $tabla = $campo_tabla[0];
            $columnas.= $campo_tabla[0].'.'.$campo_tabla[1].',';
            $encabezado[$key] = $campo_tabla[0].'.'.$campo_tabla[1];
        }
        $columnas = substr($columnas, 0, -1);
        $consulta = 'select '.$columnas.' FROM '.$tabla;
        $query = $this->db->rawQuery($consulta);
        return array('query'=>$query,'encabezado'=>$encabezado);
    }
    

}
