<?php

require_once dirname(__FILE__).'/vendor/autoload.php';
require dirname(__FILE__).'/config.php';
require dirname(__FILE__).'/classes/Consultas.php';

$builder = new SqlBuilder();
$consulta = new Consultas();
$smarty->assign(array(
    'builder' => $builder,
    'root_directory_views' => $_SERVER["REQUEST_URI"].'/views',
    'root_directory_controladores' => $_SERVER["REQUEST_URI"].'/controladores',
    'consulta' => 0,
    'tablas' => $consulta->ListarTablas(),
    'columnas' => $consulta->ListaCamposTabla('medicos'),
));
if (isset($_POST['nombres']) && isset($_POST['apellidos']) && isset($_POST['email']) && isset($_POST['telefono'])){ 
	if ($_POST['nombres']!='' && $_POST['apellidos']!='' && $_POST['email']!='' && $_POST['telefono']!=''){
		$funciones = new Funciones();
		$data = array("nombres"   => $_POST['nombres'],
	          "apellidos" => $_POST['apellidos'],
	          "email"     => $_POST['email'],
	          "telefono"  => $_POST['telefono'],
	          "ip"        => $funciones->obtenerIp(),
		);	
		$id = $db->insert('personas', $data);
		if($id){
		    $smarty->assign('consulta' , 1);
		}else{
			$smarty->assign('consulta' , 2);
		}
	}
}
$smarty->display('index.tpl');
