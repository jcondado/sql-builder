<?php

require_once dirname(__FILE__).'/vendor/autoload.php';
require dirname(__FILE__).'/config.php';
require dirname(__FILE__).'/classes/Consultas.php';

$consulta = new Consultas();
$tabla = $_POST['tabla'];
$columnas = $consulta->ListaCamposTabla($tabla);
$options = '<option value="">Seleccione</option>';
foreach ($columnas as $columna) {
	$options = $options.'<option>'.$tabla.'.'.$columna['Field'].'</option>';
}
echo $options;