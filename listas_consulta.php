<?php

require_once dirname(__FILE__).'/vendor/autoload.php';
require dirname(__FILE__).'/config.php';
require dirname(__FILE__).'/classes/Consultas.php';

$consulta = new Consultas();
$datos = $_POST['datos'];
$consulta = $consulta->Consulta($datos);
echo json_encode($consulta);
