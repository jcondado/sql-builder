<html>
	<head>
	    <!-- Required meta tags-->
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="Colorlib Templates">
	    <meta name="author" content="Colorlib">
	    <meta name="keywords" content="Colorlib Templates">

	    <!-- Title Page-->
	    <title>Consultas</title>

	    <!-- Icons font CSS-->
	    <link href="{$root_directory_views}/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
	    <link href="{$root_directory_views}/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
	    <!-- Font special for pages-->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">		
	    <!-- Main CSS-->
	    <link href="{$root_directory_views}/css/main.css" rel="stylesheet" media="all">
	    <link href="{$root_directory_views}/css/bootstrap.min.css" rel="stylesheet" media="all">
	    <link href="{$root_directory_views}/vendor/select2/select2.min.css" rel="stylesheet" media="all">
	</head>
	<body>	    
	    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
	        <div class="wrapper wrapper--w680">
	            <div class="card card-1">
	                <div class="card-heading"></div>
	                <div class="card-body">
	                    <h2 class="title">Datos de la Consulta</h2>
			            {if ($consulta eq 1)}
		                <div class="alert alert-success" role="alert">
		                    <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
		                    Consulta realizada con exito! 
		                </div>
		                {/if}
		                {if ($consulta eq 2)}
		                <div class="alert alert-danger" role="alert">
		                    <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
		                    Disculpe, ocurrio un error!
		                </div>
		                {/if}
	                    <form method="POST" action="" method="post" class="needs-validation" novalidate>
	                        <div class="row row-space">	                            
	                            <div class="col-6">
	                            	<div class="input-group">
	                                    <div class="rs-select2 js-select-simple select--no-search"  style="width: 100%">
	                                        <select name="tablas" class="tablas">
	                                        	<option value="">Seleccione</option>
	                                            <{foreach item=con from=$tablas}
												  <option value="{$con.Tables_in_prueba}">{$con.Tables_in_prueba}</option>
												{/foreach}
	                                        </select>
	                                        <div class="select-dropdown"></div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-6">
	                            	<div class="input-group">
	                                    <div class="rs-select2 js-select-simple select--no-search"  style="width: 100%">
	                                        <select name="columnas" class="columnas">
	                                        	<option value="">Seleccione</option>   
	                                        </select>
	                                        <div class="select-dropdown"></div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row row-space muestra_campos">	
	                        	<h3>Campos Seleccionados</h3>                            
	                            <div class="col-12">
	                            	<ul class="list-group campos_tablas">									  
									</ul>
	                            </div>
	                        </div>                        
	                        <div class="p-t-20">
	                        	 <div class="col-12">
		                            <button class="btn btn--radius btn--green consutar" type="button">Consultar</button>
		                        </div>
	                        </div>
	                        <div class="row row-space mt-4 muestra_resultados">	
	                        	<h3>Resultados</h3>                            
	                            <div  class="col-12 table-responsive">
	                            	<table class="table lista_registros">									  
									</table>
	                            </div>
	                        </div>   
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
		<!-- JS-->
	    <script src="{$root_directory_views}/vendor/jquery/jquery.min.js"></script>		
	    <script type="text/javascript" src="{$root_directory_views}/vendor/bootstrap/bootstrap.min.js"></script>		
	    <script src="{$root_directory_views}/vendor/datepicker/moment.min.js"></script>
    	<script src="{$root_directory_views}/vendor/datepicker/daterangepicker.js"></script>
	    <script src="{$root_directory_views}/vendor/select2/select2.min.js"></script>
	    <script src="{$root_directory_views}/js/global.js"></script>
	    <script type="text/javascript">     
	    $(".muestra_campos").hide();
	    $(".muestra_resultados").hide();
	    $('select.tablas').change(function(e) {
	    	$(".campos_tablas li").remove();
	    	$(".muestra_campos").hide();
	    	$(".muestra_resultados").hide();
		  	e.preventDefault();
		  	var tabla = $('option:selected', $(this));
		    $.ajax({
	            type: 'POST',                
	            url: "listas_campos.php",
	            data: {
	                'tabla': tabla.val(),                    
	            },
	            success: function(data) {
			        $("select.columnas option").remove();
	            	$("select.columnas").append(data);                 
	            },
	        });
		});
		$('select.columnas').change(function(e) {
		  	e.preventDefault();
		  	var campo = $('option:selected', $(this));
		  	$(".muestra_campos").show();
        	$(".campos_tablas").append('<li class="list-group-item"><span class="badge badge-pill badge-light">'+campo.val()+'</span></li>');  
		});
		$('.consutar').click(function(e) {
			datos={};
			$(".campos_tablas li").each(function (key,value) {
				datos[key] = $(this).text();
			});
            $.ajax({
	            type: 'POST',                
	            url: "listas_consulta.php",
	            data: {
	                'datos': datos,                    
	            },
	            success: function(data) {
	            	var datos = JSON.parse(data);
	            	if (datos.query.length>0){
	            		$(".lista_registros tr").remove();
	            		$(".muestra_resultados").show();
		            	var encabezado = '';
		            	var registros = '';
		            	$.each(datos.encabezado,function(key,value){
				        	encabezado = encabezado + '<th>'+value+'</th>';
				        }); 
				         $('.lista_registros').append('<tr>'+encabezado+'</tr>');   
				        $.each(datos.query,function(key,value){
				        	registros = registros + '<tr>';
				        	$.each(value,function(k,val){
				        		registros = registros + '<td>'+val+'</td>';
				        	}); 
				        	registros = registros + '</tr>';
				        });  
				        $('.lista_registros').append(registros);
			        }
	            },
	        });
		});
	    </script>
	</body>
</html>
